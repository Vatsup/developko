/*checking for new browser version, creat cookie*/
var Cookie = (function () {
	var getCookie = function (vcookie) {
		var cookie = " " + document.cookie;
		var search = " " + vcookie + "=";
		var setStr = null;
		var offset = 0;
		var end = 0;
		if (cookie.length > 0) {
			offset = cookie.indexOf(search);
			if (offset != -1) {
				offset += search.length;
				end = cookie.indexOf(";", offset)
				if (end == -1) {
					end = cookie.length;
				}
				setStr = unescape(cookie.substring(offset, end));
			}
		}
		return(setStr);
	};
	var setCookie = function (vcookie, value, expires, path, domain, secure) {
		var d = new Date();
		var now = d.getDate();
		var expires = ( expires ? d.setDate(now + expires) : d.setDate(now - 1) );
		document.cookie = vcookie + "=" + escape(value) +
			((expires) ? "; expires=" + expires : "") +
			((path) ? "; path=" + path : "") +
			((domain) ? "; domain=" + domain : "") +
			((secure) ? "; secure" : "");
	};
	return {
		set:setCookie,
		get:getCookie
	}
})();

/*checking for new browser version, creat cookie end*/

$(document).ready(function () {
    $('body').on('click','.innerlinkBlack',function(){
        $(this).next('.toggleBlock').fadeToggle('fast')
    })









    $('input, select').styler();

	/*inisialization script for input with placeholder*/
	placeholders();
	/*всплывашка мелкая */
    $('body').on('click', '.activationPopup', function(event){
        function resetpopup() {
            $(".activationPopup").removeClass("active").addClass("passive");
            $(".popup-menu").addClass("none");
        }
        function bodyclickf() {
            $('body').click(function () {
                $('.popup-menu').addClass('none');
                $('.activationPopup').removeClass('active').addClass('passive');
            })
        }
        if ($(this).hasClass("passive")) {
            resetpopup()
            $(this).removeClass("passive").addClass("active");
            $(this).next(".popup-menu").toggleClass("none");
            event.stopPropagation();
            bodyclickf()
        } else {
            resetpopup()
            event.stopPropagation();
            bodyclickf()
        }
        $('.user_info_norm .icon').click(function () {
            resetpopup()
        });
    });


    $('body').on('click', '.popup-menu', function(e){
        e.stopPropagation();
    })
	/*всплывашка мелкая end*/

	/*checking for new browser version, set cookie*/
	function foo() {
		Cookie.set('vcookie', 1, 1 / 12);
		console.log(Cookie.get('vcookie'));
	};
	/*checking for new browser version, read cookie*/
	var showPopup = 1;
	if (Cookie.get('vcookie') == 1) {
		showPopup = 0;
	}
	;
	/*checking for new browser version, show pop-up*/
	if (showPopup) {
		checkBrowser(params = {
			warning:       'уже немного устарела!',
			message:       'Из-за этого некоторые элементы сайта могут не корректно отображатся.',
			question:      'Не хотите ли Вы обновить свой браузер?',
			chrome:        18,
			safari:        5,
			msie:          9,
			opera:         11,
			firefox:       12,
			chromeLink:    'http://www.google.com/chrome',
			safariLink:    'http://www.apple.com/ru/safari',
			msieLink:      'http://www.microsoft.com/rus/windows',
			operaLink:     'http://ru.opera.com',
			firefoxLink:   'http://www.mozilla-russia.org/products',
			cancelCallback:foo
		});
	}
	;
	/*checking for new browser version, show pop-up end*/
});

function placeholders() {
	var placeholder = $('input[placeholder], textarea[placeholder]');
	if (placeholder.length) {
		placeholder.placeholder();
	}
}
